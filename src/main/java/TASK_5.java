import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TASK_5 {

    HashMap<String, MyTime> hashMap;

    public TASK_5(){this.hashMap = new HashMap<>();}

    public static LocalDateTime getTime(String data){
        String[] parseData = data.split("\\s");

        String[] date = parseData[0].split("-");

        int year = Integer.parseInt(date[0]);
        int month = Integer.parseInt(date[1]);
        int day = Integer.parseInt(date[2]);
        String[] time = parseData[1].split(":");
        int hour = Integer.parseInt(time[0]);
        int minute = Integer.parseInt(time[1]);
        int second = Integer.parseInt(time[2]);

        LocalDateTime returnTime = LocalDateTime.of(year,month,day,hour,minute,second);

        return returnTime;
    }

    public HashMap<String,MyTime> doTask5(List<MonitoredData> data,List<String> activities){
        List<String> sortedActivities = new ArrayList<>();
        int sortedIndex = 0;

        activities.stream()
                .distinct()
                .sorted()
                .forEach(x->sortedActivities.add(x));

        while(sortedIndex < sortedActivities.size()){

            long seconds = 0;
            long minutes = 0;
            long hours = 0;

            for(int i = 0 ; i < data.size(); i++){

                if(activities.get(i).equals(sortedActivities.get(sortedIndex))){

                    LocalDateTime startTime = getTime(data.get(i).startTime);
                    LocalDateTime endTime = getTime(data.get(i).endTime);

                    seconds  += Duration.between(startTime,endTime).toSeconds();
                    minutes += seconds / 60;
                    seconds %= 60;

                    hours += minutes / 60;
                    minutes %= 60;
                }
            }


            hashMap.put(sortedActivities.get(sortedIndex),new MyTime(seconds,hours,minutes));
            sortedIndex++;
        }

        return hashMap;
    }

    public void printResult() throws FileNotFoundException {

        PrintWriter pw = new PrintWriter("Task_5.txt");
        hashMap.forEach((activityLabel,MyTime) ->
                    pw.println(activityLabel + " --- Duration: "  + MyTime.hours +" hours " + MyTime.minutes +" minutes "+ MyTime.seconds + " seconds "));

        pw.close();
    }
}
