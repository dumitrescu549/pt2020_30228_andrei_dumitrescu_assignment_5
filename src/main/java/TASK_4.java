import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TASK_4 {

    HashMap<Integer, Map<String,Integer>> hashMap;
    List<MonitoredData> data;
    List<Integer> days = new ArrayList<>();

    public TASK_4(List<MonitoredData> data){

        this.hashMap = new HashMap<>();
        this.data = data;

    }

    public void parseStartTime(){ // in aceasta metoda adaugam intr-o lista toate datele
        for(int i = 0 ; i < data.size();i++){
            String[] parseData = data.get(i).startTime.split("-");
            String[] parseData2 = parseData[2].split("\\s");
            days.add(Integer.parseInt(parseData2[0]));
        }
    }

    public HashMap<Integer,Map<String,Integer>> doTask4(List <String> activityLabel){

        List<String> activityNames = new ArrayList<String>();

        int index = 0;
        int daysIndex = 0;
        int currentDay = days.get(daysIndex);


        activityLabel.stream() // numele fiecarei activitati in parte
                .distinct()
                .sorted()
                .forEach(x-> activityNames.add(x));


        while(index < data.size() -1 ){

            List<String> activitiesForTheDay = new ArrayList<>(); // stringul in care salvam toate activitatile facute intr-o zi

            while(currentDay == days.get(daysIndex) && daysIndex < days.size() -1){
                activitiesForTheDay.add(activityLabel.get(index));
                daysIndex++;
                index++;
            }

            List <String> sortedList = new ArrayList();
            activitiesForTheDay.stream() // punem in ordine activitatile facute intr-o zi
                    .sorted()
                    .forEach(x->sortedList.add(x));

            int sortedIndex = 0;
            int activityIndex = 0;
            int count = 0;
            HashMap<String,Integer> hashMap2 = new HashMap<>();

            while(sortedIndex < sortedList.size()){
                if(sortedList.get(sortedIndex).equals(activityNames.get(activityIndex))){
                    count ++;
                    sortedIndex++;
                }
                else{
                    hashMap2.put(activityNames.get(activityIndex),count);
                    activityIndex ++;
                    sortedIndex ++;
                    count = 1;
                }
            }
            hashMap2.put(activityNames.get(activityIndex),count);

            hashMap.put(currentDay,hashMap2);
            currentDay = days.get(daysIndex);

        }

        return hashMap;
    }

    public void printResult() throws FileNotFoundException { // scrierea in fisier
        PrintWriter pw = new PrintWriter("Task_4.txt");

        hashMap.forEach((day,hashmap2) ->{

                    pw.println("Day " + day);
                    pw.println();
                    hashmap2.forEach((activityLabel,noOfApperances) ->
                        pw.println(activityLabel + " --- " + noOfApperances));
                    pw.println();
                }

        );
        pw.close();
    }

}
