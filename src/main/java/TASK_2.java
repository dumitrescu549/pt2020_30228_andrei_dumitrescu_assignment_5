import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TASK_2 {

    int count = 0;

    public void doTask2(List<String> days) throws FileNotFoundException {
        List<String> areDistinct = new ArrayList<String>();

        days.stream() // adaugam in sirul areDistinct doar datele care reprezinta ziua
                .map(x-> x.split(" "))
                .map(y -> y[0].split("-"))
                .forEach(y->areDistinct.add(y[2]));


        areDistinct.stream() // vedem daca datele respective sunt distincte -> daca sunt facem ++ pt contor
                .distinct()
                .forEach(x-> count++);

        // afisare in fisier
        PrintWriter pw = new PrintWriter("Task_2.txt");
        pw.println("Number of distinct days from monitoring data: " + count);
        pw.close();

    }
}
