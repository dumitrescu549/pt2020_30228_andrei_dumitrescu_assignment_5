import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TASK_6 {

    List<String> result;

    public TASK_6(){this.result = new ArrayList<String>();}

    public void doTask6(List<String> activities,List<MonitoredData>data){

        List<String> sortedActivities = new ArrayList<>();
        int sortedIndex = 0;
        List <Integer> timeArray = new ArrayList<>();
        List <Integer> under5Min = new ArrayList<>();

        activities.stream()
                .distinct()
                .sorted()
                .forEach(x->sortedActivities.add(x));

        while(sortedIndex < sortedActivities.size()){

            long seconds = 0;
            long minutes = 0;
            int count = 0;


            for(int i = 0 ; i < data.size(); i++){

                if(activities.get(i).equals(sortedActivities.get(sortedIndex))){

                    LocalDateTime startTime = TASK_5.getTime(data.get(i).startTime);
                    LocalDateTime endTime = TASK_5.getTime(data.get(i).endTime);

                    seconds  += Duration.between(startTime,endTime).toSeconds();
                    minutes += seconds / 60;
                    seconds %= 60;

                    timeArray.add((int)minutes);
                }
            }

            timeArray.stream()
                    .filter(min -> min < 5)
                    .forEach(x-> under5Min.add(x));


            float perc = (float)under5Min.size() / (float)timeArray.size();

            if(perc > 0.9){
                result.add(sortedActivities.get(sortedIndex));
            }

            timeArray.clear();
            sortedIndex++;
        }

    }


    public void printResult() throws FileNotFoundException {
        PrintWriter pw = new PrintWriter("Task_6.txt");
        result.stream()
                .forEach(x->pw.println(x));
        pw.close();
    }
}
