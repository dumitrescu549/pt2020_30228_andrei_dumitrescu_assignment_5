import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TASK_3 {
    HashMap<String, Integer> hashMap;

    public TASK_3(){
        this.hashMap = new HashMap<>();
    }

    public HashMap<String,Integer> doTask3(List<String> activityLabel){
        List<String> activityNames = new ArrayList<String>();
        List<String> sortedArray = new ArrayList<String>();
        int sortedIndex = 0;
        int count = 0;
        int activityIndex = 0;

        activityLabel.stream() // numele fiecarei activitati in parte
                .distinct()
                .sorted()
                .forEach(x-> activityNames.add(x));

        activityLabel.stream() // sortam sir-ul a.i sa avem toate activitatile una dupa alta
                .sorted()
                .forEach(x-> sortedArray.add(x));



        while(sortedIndex < sortedArray.size()){ // algoritm de numarare al fiecarui nume de activitate in parte

            if(sortedArray.get(sortedIndex).equals(activityNames.get(activityIndex))){

                count ++;
                sortedIndex++;
            }
            else
            {

                hashMap.put(activityNames.get(activityIndex),count);

                activityIndex ++;
                sortedIndex ++;
                count = 1;

            }
        }
        hashMap.put(activityNames.get(activityIndex),count);
        return hashMap;
    }

    public void printResult() throws FileNotFoundException { // scrierea in fisier
        PrintWriter pw = new PrintWriter("Task_3.txt");
        hashMap.forEach((activityLabel,noOfApperances) ->
                pw.println(activityLabel + " --- " + noOfApperances));
        pw.close();
    }
}


