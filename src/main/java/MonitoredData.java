public class MonitoredData {

    String startTime;
    String endTime;
    String activityLabel;

    public MonitoredData(String startTime, String endTime, String activityLabel){
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityLabel = activityLabel;
    }
}
