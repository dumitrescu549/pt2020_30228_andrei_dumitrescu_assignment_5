import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class TASK_1 {
    File file;
    List<MonitoredData> data;

    public TASK_1(File file, List<MonitoredData> data){
        this.file = file;
        this.data = data;
    }

    public List<MonitoredData> doTask1() throws IOException {

        Files.lines(Paths.get(file.toURI())) // luam cate un o linie din fisier
                .map(x->x.split("\\s\\t")) // o parsam
                .forEach(x -> data.add(new MonitoredData(x[0],x[1],x[2]))); // introducem datele in sir


        PrintWriter pw = new PrintWriter("Task_1.txt");

        for (MonitoredData md: data) {
            pw.println(md.startTime + " --- " + md.endTime + " --- " +md.activityLabel);
        }

        pw.close();

        return data;
    }

}
