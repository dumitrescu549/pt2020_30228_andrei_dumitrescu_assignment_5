import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        List<MonitoredData> data = new ArrayList<MonitoredData>();
        File file = new File(args[0]);

        //Task_1
        TASK_1 task1 = new TASK_1(file,data);
        data = task1.doTask1();

        //Task_2
        List<String> days = new ArrayList<String>();
        for(int i = 0 ; i < data.size();i++){
            days.add(data.get(i).startTime);
            days.add(data.get(i).endTime);
        }

        TASK_2 task2 = new TASK_2();
        task2.doTask2(days);

        //Task_3
        List<String> activities = new ArrayList<String>();
        for(int i = 0 ; i < data.size(); i++){
            String[] parseData = data.get(i).activityLabel.split("\\s");
            activities.add(parseData[0]);
        }
        TASK_3 task3 = new TASK_3();
        task3.doTask3(activities);
        task3.printResult();

        //Task_4

        TASK_4 task4 = new TASK_4(data);
        task4.parseStartTime();
        task4.doTask4(activities);
        task4.printResult();

        //Task_5

        TASK_5 task5 = new TASK_5();
        task5.doTask5(data,activities);
        task5.printResult();

        //TASK_6

        TASK_6 task6 = new TASK_6();
        task6.doTask6(activities,data);
        task6.printResult();




    }
}
